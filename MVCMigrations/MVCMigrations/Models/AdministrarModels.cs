﻿using System.Data.Entity;

namespace MVCMigrations.Models
{
    public class AdministrarModels : DbContext
    {
        //Inicializamos la BDD por defecto
        public AdministrarModels() 
            : base("conGlobal")
        {
        }

        //Inicializamos las Tablas dentro de la Base de Datos
        public DbSet<Productos> Productos { set; get; }
    }
}