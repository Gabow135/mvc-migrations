﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MVCMigrations.Models
{
    public class Productos
    {
        public int id { set; get; }
        public string nombre { set; get; }
        public int cantidad { set; get; }
    }
}