namespace MVCMigrations.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateProductos : DbMigration
    {
        //Lo que va hacer el update
        public override void Up()
        {
            CreateTable(
                "dbo.Products",
                c => new
                {
                    id = c.Int(nullable: false, identity: true),
                    nombre = c.String(nullable: false),
                    cantidad = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.id);
        }
        //Lo que va hacer si hay algun error
        public override void Down()
        {
            DropTable("dbo.Products");
        }
    }
}
